from tkinter.messagebox import *
from tkinter import *
import string

root = Tk()
root.title("CHIFFRE DE CESAR")
root.minsize(600, 500)

valeur_decalage= IntVar()
valeur_decalage.set(8)

alphabet_a_A = string.ascii_lowercase + string.ascii_uppercase

#Fonction qui recupere la saisie de l'utilisateur
def get_entree_content():

    return entree.get("0.0", END).strip()

#Fonction qui verifie la saisie de l'utilisateur
def check_saisie():

    showwarning(title="ATTENTION",message="Ecrit un message!!")

#Fonction de chiffrage non circulaire
def chiffrer_non_circulairement():
    if(get_entree_content()==''):
        check_saisie()
    else:
        liste_message = []
        for i in get_entree_content():
            i = ord(i)+valeur_decalage.get()
            liste_message.append(chr(i))
            sortie.delete(0.0,END)
            listtochaine = "".join(liste_message)
            sortie.insert(0.0,listtochaine)

#Fonction de dechiffrage non circulaire
def dechiffrer_non_circulairement():
    if(get_entree_content()==''):
        check_saisie()
    else:
        liste_message = []
        for i in get_entree_content():
            i = ord(i)-valeur_decalage.get()
            liste_message.append(chr(i))
            sortie.delete(0.0,END)
            listtochaine = "".join(liste_message)
            sortie.insert(0.0,listtochaine)

#Fonction de chiffrage circulaire
def chiffrer_circulairement():
    if(get_entree_content()==''):
        check_saisie()
    if(get_entree_content().isalpha()==False):
        showwarning(title="ATTENTION",message="Pas de caractères spéciaux!!")
    else:
        liste_message = []
        alphabet_a_A_chiffrer = alphabet_a_A[valeur_decalage.get():] + alphabet_a_A[:valeur_decalage.get()]
        for i in get_entree_content():
            lettre = (alphabet_a_A.index(i) - 52)
            liste_message.append(alphabet_a_A_chiffrer[lettre])
            sortie.delete(0.0,END)
            listtochaine = "".join(liste_message)
            sortie.insert(0.0,listtochaine)

#Fonction de dechiffrage circulaire
def dechiffrer_circulairement():
    if(get_entree_content()==''):
        check_saisie()
    if(get_entree_content().isalpha()==False):
        showwarning(title="ATTENTION",message="Pas de caractères spéciaux!!")
    else:
        liste_message = []
        alphabet_a_A_chiffrer = alphabet_a_A[valeur_decalage.get():] + alphabet_a_A[:valeur_decalage.get()]
        alphabet_a_A_dechiffrer = alphabet_a_A_chiffrer[:valeur_decalage.get()] + alphabet_a_A_chiffrer[valeur_decalage.get():]
        for i in get_entree_content():
            lettre = (alphabet_a_A_dechiffrer.index(i) - 52)
            liste_message.append(alphabet_a_A[lettre])
            sortie.delete(0.0,END)
            listtochaine = "".join(liste_message)
            sortie.insert(0.0,listtochaine)

#definition de nos widgets
label_info = Label(text="Saisie ton texte a chiffrer/dechiffrer")
label_info.grid(row=1,column=1)

entree = Text(root,wrap='word', width=30,heigh=10)
entree.grid(row=2, column=1)

change_decalage = Entry(root,textvariable=valeur_decalage, width=6)
change_decalage.grid(row=3,column=2)

label_info = Label(text="Voici ton texte a chiffrer/dechiffrer")
label_info.grid(row=3,column=1)

sortie = Text(root,wrap='word', width=30,heigh=10)
sortie.grid(row=4, column=1)

btn_chiffrer_non_circulairement = Button(root,text='Chiffrer non cirulairement',command=chiffrer_non_circulairement)
btn_chiffrer_non_circulairement.grid(row=4, column=0)

btn_dechiffrer_non_circulairement = Button(root,text='Dechiffrer non cirulairement',command=dechiffrer_non_circulairement)
btn_dechiffrer_non_circulairement.grid(row=4, column=2)

btn_chiffrer_circulaire = Button(root,text='Chiffrer circulairement',command=chiffrer_circulairement)
btn_chiffrer_circulaire.grid(row=5, column=0)

btn_dechiffrer_circulaire = Button(root,text='Dechiffrer circulairement',command=dechiffrer_circulairement)
btn_dechiffrer_circulaire.grid(row=5, column=2)

#Configuration de nos lignes et colonnes
root.rowconfigure(0, weight=1)
root.rowconfigure(1, weight=1)
root.rowconfigure(2, weight=1)
root.rowconfigure(3, weight=1)
root.rowconfigure(4, weight=1)
root.rowconfigure(5, weight=1)
root.columnconfigure(0, weight=1)
root.columnconfigure(1, weight=1)
root.columnconfigure(2, weight=1)

root.mainloop()